<?php

return [
    'report' => 'Report',
    'in_months' => 'In Months',
    'in_weeks' => 'In Weeks',
    'view_yearly_label' => 'View Yearly',
    'view_report' => 'View Report',
    'this_year' => 'This Year',
    'graph' => 'Summary Graph',
    'detail' => 'Report Detail',
    'view_monthly' => 'View Monthly',
    'monthly' => 'Report Month :year_month',
    'view_weekly' => 'View Weekly',
    'weekly' => 'Report Week :year_week',
];
